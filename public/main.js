$(function(){

	var game = {
		nick: '',
		myID: null,
		opponent: '',
		chances: 10,
		guessing: false,
		lettersLeft: 23,
		updateRoom: function(room){
			var content = '' + room.p1.score + ' <b>' + room.p1.name + '</b> - ';
			if (room.p2.id != null)  // w pokoju jest dwóch graczy
				content += '<b>' + room.p2.name + '</b> ' + room.p2.score + '';
			else if (room.p1.id != game.myID)  // ktoś utworzył nowy pokój
			{
				if ($('#newRoom').is(":visible"))  // gracz oczekuje
					content += '??? <button class="roomBtn">Dołącz</button>';
				else // gracz nie podał nicku lub prowadzi rozgrywkę
					content += '??? <button class="roomBtn" style="display:none">Dołącz</button>';
			}
			else // to jest mój nowy pokój :)
				content += '<i>Czekaj...</i>';

			var r = $('#rooms > div[rel="' + room.p1.id + '"]');
			if (r.length > 0)
				r.html(content);
			else
				$('#rooms').append('<div rel="' + room.p1.id + '">' + content + '</div>');

			game.refreshRoomsCounter();
		},
		removeRoom: function(id){
			$('#rooms > div[rel="' +id + '"]').remove();
			game.refreshRoomsCounter();
		},
		refreshRoomsCounter: function(){
			$('#roomCounter').text($('#rooms > div').length);
		},
		resetRoom: function(){
			var x = $('#playersAndScores > div');
			x.eq(0).text('0');
			x.eq(1).text('???');
			x.eq(2).text('???');
			x.eq(3).text('0');
			$('#gameStatus').html('');
			game.resetBoard();
		},
		resetBoard: function(){
			$('#secretWord').html('').hide();
			$('#wisielec > div').removeClass('drawn');
			$('#keyboard').addClass('locked').removeClass('active');
			$('#keyboard > div > div').removeClass('used');
			var x = $('#stats td');
			x.eq(0).text('10');
			x.eq(1).text('0');
			x.eq(2).text('23');
			game.chances = 10;
			game.lettersLeft = 23;
		},
		initGameWait: function(){  // stowrzyłem pokój i czekam na przeciwnika
			game.resetRoom();
			$('#rooms > div > button, #newRoom').hide();
			$('#playersAndScores > div:eq(1)').text(game.nick);
			$('#gameStatus').html('Zaczekaj na drugiego gracza...');
			$('#game').slideDown();
		},
		initGameJoin: function(room){  // dołączam się do czyjegoś pokoju
			game.resetRoom();
			$('#rooms > div > button, #newRoom').hide();
			$('#playersAndScores > div:eq(1)').text(room.p1.name);
			$('#playersAndScores > div:eq(2)').text(room.p2.name);
			game.opponent = room.p1.name;
			//$('#gameStatus').html(game.opponent + ' wymyśla dla Ciebie hasło, daj mu chwilę ;)');
			$('#game').slideDown();
			game.startRound(false);
		},
		initGameBegin: function(opponentName){  // drugi gracz wszedł do pokoju
			game.opponent = opponentName;
			$('#playersAndScores > div:eq(2)').text(opponentName);
			$('#gameStatus').html(opponentName + ' dołączył do gry. Wymyśl dla niego hasło!');
			game.startRound(true);
		},
		startRound: function(nowIGiveTheWord){
			game.resetBoard();
			if (nowIGiveTheWord)
			{
				game.guessing = false;

				game.popupNewSecret(function(secret){
					socket.emit('newSecret', secret);

					$('#gameStatus').html(game.opponent + ' zgaduje Twoje hasło: <b>' + secret.toUpperCase() + '</b>');
					game.createSecretWordField(secret.length);
					$('#keyboard').removeClass('locked');
				});

				/*
				var secret = '';
				while (secret == '' || secret.length < 2)
				{
					secret = $.trim(prompt('Podaj hasło dla drugiego gracza:'));
					if (secret.length < 2)
						alert('Minimum 2 znaki!');
				}

				socket.emit('newSecret', secret);

				$('#gameStatus').html(game.opponent + ' zgaduje Twoje hasło: <b>' + secret.toUpperCase() + '</b>');
				game.createSecretWordField(secret.length);
				$('#keyboard').removeClass('locked');
				*/
			}
			else
			{
				$('#gameStatus').html(game.opponent + ' wymyśla dla Ciebie hasło, daj mu chwilę ;)');
			}
		},
		createSecretWordField: function(lettersCount){
			$('#secretWord').html(new Array(lettersCount+1).join('<div></div>')).show();
			game.fieldsToFill = lettersCount;
		},
		startGuessing: function(){
			$('#keyboard').removeClass('locked').addClass('active');
			$('#gameStatus').html(game.opponent + ' wymyślił dla Ciebie hasło, zgaduj!');
			game.guessing = true;
		},
		guessResult: function(letter, positions){
			var letterUC = letter.toUpperCase();
			if (positions.length > 0)  // litera występuje w haśle
			{
				var divs = $('#secretWord > div');
				$.each(positions, function(k, v){
					divs.eq(v).text(letterUC);
				});
				game.fieldsToFill = game.fieldsToFill - positions.length;
				$('#stats td:eq(1)').text(game.fieldsToFill);
			}
			else
			{
				$('#wisielec > div').eq(10-game.chances).addClass('drawn');
				$('#stats td:eq(0)').text(--game.chances);
			}

			if (!game.guessing)
				$('#keyboard > div > div:contains("' + letterUC + '")').addClass('used');
			
			$('#stats td:eq(2)').text(--game.lettersLeft);

		},
		secretGuessed: function(room){
			$('#playersAndScores > div:eq(0)').text(room.p1.score);
			$('#playersAndScores > div:eq(3)').text(room.p2.score);

			if (game.guessing)
			{
				$('#gameStatus').html('Wygrałeś.');
				$('.dim.win').fadeIn('fast');
				//popup("Odgadłeś, brawo!\n\n" + room.p1.name + ': ' + room.p1.score + "\n" + room.p2.name + ': ' + room.p2.score);
			}
			else
			{
				$('#gameStatus').html("Kurde, " + game.opponent + " odgadł hasło :(");
				//popup("Kurde, " + game.opponent + " odgadł hasło :(\n\n" + room.p1.name + ': ' + room.p1.score + "\n" + room.p2.name + ': ' + room.p2.score);

			}
			setTimeout(function(){
				game.startRound(game.guessing);
			}, 2000);
		},
		gameOver: function(word, room){
			if (game.guessing)
			{
				$('#gameStatus').html('Przegrałeś.');
				$('.dim.lose').fadeIn('fast');
				//popup("Zawisłeś!\n\nHasło to: " + word.toUpperCase() + "\n\n" + room.p1.name + ': ' + room.p1.score + "\n" + room.p2.name + ': ' + room.p2.score);
			}
			else
			{
				$('#gameStatus').html(game.opponent + " zawisł :D");
				//popup(game.opponent + " zawisł!\n\n" + room.p1.name + ': ' + room.p1.score + "\n" + room.p2.name + ': ' + room.p2.score);
			}
			setTimeout(function(){
				game.startRound(game.guessing);
			}, 2000);
		},
		letterPressed: function(letter){
			if (!game.guessing || !$('#keyboard').hasClass('active')) return;
			var kayboardBtn = $('#keyboard > div > div:contains("' + letter.toUpperCase() + '")');
			if (kayboardBtn.hasClass('used')) return;

			kayboardBtn.addClass('used');
			socket.emit('guess', letter.toLowerCase());
		},
		popupNewSecret: function(callback){
			game.popupNewSecret.cb = callback;
			$('.dim.secret').fadeIn('fast');
			$('.dim.secret span').text(game.opponent);
			$('.dim.secret input').val('').focus();
		}
	}




	/* ######################### */
	/* ####### socket.io ####### */
	/* ######################### */

	var socket = io.connect();

	socket.on('connect', function(){
		$('#laczenie').fadeOut();
		$('#main1').fadeIn();
		$('#inputNick').focus();
	});

	socket.on('disconnect', function(){
		//alert('Oj, serwer padł :(');
	});

	socket.on('welcome', function(myID, rooms){  // pobranie identyfikatora i aktualnej listy pokojów
		game.myID = myID;
		$.each(rooms, function(k, room){
			game.updateRoom(room);
		});
	});

	socket.on('roomUpdate', function(room){
		game.updateRoom(room);
	});

	socket.on('roomRemove', function(id){
		game.removeRoom(id);
	});

	socket.on('roomJoin', function(room){
		game.initGameJoin(room);
	});

	socket.on('newSecret', function(len){
		game.createSecretWordField(len);
		game.startGuessing();
	});

	socket.on('roomLeaved', function(breakerID, room){  // jeden z graczy opuścił pokój (w dowolny sposób)
		if (breakerID != null)  // trwała rozgrywka
		{
			var s;
			if (breakerID == game.myID)
				$('.dim.leave h2').text('Opuściłeś pokój.');
			else if (breakerID == room.p1.id)
				$('.dim.leave h2').text(room.p1.name + ' opuścił pokój :(');
			else
				$('.dim.leave h2').text(room.p2.name + ' opuścił pokój :(');

			var divs = $('.dim.leave .popup > div');
			divs.eq(0).text(room.p1.score);
			divs.eq(1).text(room.p1.name);
			divs.eq(2).text(room.p2.name);
			divs.eq(3).text(room.p2.score);

			$('.dim.leave').fadeIn('fast');
			$('.dim.secret').hide();
		}

		$('#game').slideUp('fast');
		$('#rooms > div > button, #newRoom').show();
	});

	socket.on('opponentJoined', function(opponentName){
		game.initGameBegin(opponentName);
	});

	socket.on('guessResult', function(letter, positions){
		game.guessResult(letter, positions);
	});

	socket.on('secretGuessed', function(room){
		game.secretGuessed(room);
	});

	socket.on('gameOver', function(word, room){
		game.gameOver(word, room);
	});


	/* ############################# */
	/* ####### PODANIE NICKU ####### */
	/* ############################# */


	$('#inputNick').keypress(function(e){
		if (e.which == 13)  // enter
			$('#buttonNick').trigger('click');
	});

	$('#buttonNick').click(function(){
		var nick = $.trim($('#inputNick').val());
		if (nick.length < 3)
		{
			alert('Podaj minimum 3 znaki!');
			$('#inputNick').focus();
		}
		else
		{
			game.nick = nick;
			socket.emit('addPlayer', nick);
			$('#nick').text(nick);
			//$('#game').slideDown('slow');
			$('#inputNick, #buttonNick').remove();
			$('#rooms > div > button, #newRoom').show();
		}
	});




	/* ###################### */
	/* ####### POKOJE ####### */
	/* ###################### */

	$('#newRoom').click(function(){
		$(this).hide();
		$('#rooms > div > button').hide();
		$('#removeRoom').show();
		socket.emit('newRoom');
		game.initGameWait();
	});

	$('#btnLeaveRoom').click(function(){
		socket.emit('leaveRoom');
	});


	// join
	$('#rooms').on('click','> div button', function(){
		var roomID = $(this).parent().attr('rel');
		socket.emit('joinRoom', roomID);
	});




	/* ########################## */
	/* ####### KLAWIATURA ####### */
	/* ########################## */

	$('#keyboard > div > div').click(function(){
		game.letterPressed($(this).text());
	});

	$(document).keypress(function(e){
		var c = e.charCode;
		if ((c >= 97 && c <= 122) || c==261  || c==263  || c==281  || c==322  || c==324  || c==243  || c==347  || c==378  || c==380)  // >=a, <=z + polskie ogonki 
		//  || c==161 || c==198 || c==202 || c==163 || c==209 || c==211 || c==166 || c==172 || c==175 || c==177 || c==230 || c==234 || c==179 || c==241 || c==243 || c==182 || c==188 || c==191
			game.letterPressed(String.fromCharCode(c));
	});


	/* ###################### */
	/* ####### POPUPY ####### */
	/* ###################### */


	$('.dim.secret button').click(function(){
		var v = $.trim($('.dim.secret input').val());
		if (v.length < 2)
		{
			alert('Minimum 2 znaki!');
			return;
		}

		$('.dim.secret').fadeOut('fast');
		if (game.popupNewSecret.cb && typeof game.popupNewSecret.cb == 'function')
			game.popupNewSecret.cb(v);
	});

	$('.dim.secret input').keypress(function(e){
		if (e.which == 13)
		{
			$('.dim.secret button').trigger('click');
			return;
		}
		var c = e.charCode || e.keyCode || 0;
		if (!(c==8 || c==9 || c==45 || c==46 || (c >= 35 && c <= 40) || (c >= 97 && c <= 122) || c==261  || c==263  || c==281  || c==322  || c==324  || c==243  || c==347  || c==378  || c==380))  //strzałki, home, end, backspace, delete, insert, tab, literki - te są ok
			return false;
	});


	$('.dim.leave button').click(function(){
		$('.dim.leave').fadeOut('fast');
	});

	$('.dim.win button').click(function(){
		$('.dim.win').fadeOut('fast');
	});

	$('.dim.lose button').click(function(){
		$('.dim.lose').fadeOut('fast');
	});

});
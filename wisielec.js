var express = require('express'),
	app = express(),
	http = require('http'),
	server = http.createServer(app),
	io = require('socket.io').listen(server, {log: false});

server.listen(8887);

console.log('Listening...');

app.use(express.static(__dirname + '/public'));


var rooms = {};
var rounds = {};

io.sockets.on('connection', function (socket) {

	/* ##################################### */
	/* ####### NAWIĄZANIE POŁĄCZENIA ####### */
	/* ##################################### */

	socket.room = null;
	socket.emit('welcome', socket.id, rooms);




	/* ############################# */
	/* ####### PODANIE NICKU ####### */
	/* ############################# */

	socket.on('addPlayer', function(playername){  // gracz podał swój nick
		socket.playername = playername;
		console.log('New player:', playername);
	});




	/* ###################### */
	/* ####### POKOJE ####### */
	/* ###################### */

	socket.on('newRoom', function(){
		console.log('new room -', socket.playername, '('+socket.id+')');

		rooms[socket.id] = {
			p1: {
				id: socket.id,
				name: socket.playername,
				score: 0
			},
			p2: {
				id: null,
				name: '',
				score: 0
			}
		};

		// (id pokoju to id gracza, który go utworzył)
		socket.room = socket.id;
		socket.join(socket.room);

		// do wszystkich
		io.sockets.emit('roomUpdate', rooms[socket.id]);
	});

	socket.on('joinRoom', function(roomID){
		console.log('JOIN room -', rooms[roomID].p1.name, 'vs.', socket.playername);

		rooms[roomID].p2 = {
			id: socket.id,
			name: socket.playername,
			score: 0
		};

		socket.room = roomID;
		socket.join(roomID);

		// do wszystkich
		io.sockets.emit('roomUpdate', rooms[roomID]);

		socket.emit('roomJoin', rooms[roomID]);
		io.sockets.socket(rooms[roomID].p1.id).emit('opponentJoined', socket.playername);
	});

	socket.on('leaveRoom', function(){
		var _room = socket.room;
		io.sockets.emit('roomRemove', _room);


		if (rooms[_room].p2.id != null)  // gracz nie był w pokoju sam - grał z kimś
		{
			io.sockets.in(_room).emit('roomLeaved', socket.id, rooms[_room]);

			io.sockets.socket(rooms[_room].p2.id).leave(_room);
			io.sockets.socket(rooms[_room].p2.id).room = null;
			
		}
		else   // gracz był w pokoju sam
		{
			socket.emit('roomLeaved', null, null);
		}

		io.sockets.socket(rooms[_room].p1.id).leave(_room);
		io.sockets.socket(rooms[_room].p1.id).room = null;
		

		delete rooms[_room];

		if (typeof rounds[_room] != 'undefined')
			delete rounds[_room];
	});


	socket.on('disconnect', function(){
		console.log('player disconnected:', socket.id);

		if (socket.room != null)  // gracz był w pokoju
		{
			socket.broadcast.emit('roomRemove', socket.room);

			if (rooms[socket.room].p2.id != null)  // nie był w pokoju sam - grał z kimś
			{
				io.sockets.in(socket.room).emit('roomLeaved', socket.id, rooms[socket.room]);

				if (socket.room == socket.id)  // był w utworzonym przez siebie pokoju
				{
					io.sockets.socket(rooms[socket.room].p2.id).leave(socket.room);
					io.sockets.socket(rooms[socket.room].p2.id).room = null;
				}
				else
				{
					io.sockets.socket(socket.room).leave(socket.room);
					io.sockets.socket(socket.room).room = null;
				}

				if (typeof rounds[socket.room] != 'undefined')
					delete rounds[socket.room];
			}

			delete rooms[socket.room];
		}
	});



	/* ######################### */
	/* ####### ROZGRYWKA ####### */
	/* ######################### */

	socket.on('newSecret', function(secret){
		console.log(socket.playername, 'set new secret word:', secret)

		var s = secret.toLowerCase();
		rounds[socket.room] = {
			word: s,
			chances: 10,
			fieldsToFill: s.length
		};

		socket.broadcast.to(socket.room).emit('newSecret', s.length);
	});

	socket.on('guess', function(letter){
		//console.log(socket.playername, 'guessing letter:', letter);

		var word = rounds[socket.room].word;
		var letterLC = letter.toLowerCase();  // dla pewności
		var positions = [];
		for (var i=0; i<word.length; i++)
			if (word.charAt(i) == letterLC)
				positions.push(i);

		io.sockets.in(socket.room).emit('guessResult', letterLC, positions);  // do obydwu graczy

		if (positions.length > 0)  // odgadnięto literę
		{
			console.log(socket.playername, 'guessing letter:', letter, '+' + positions.length);

			rounds[socket.room].fieldsToFill = rounds[socket.room].fieldsToFill - positions.length;
			if (rounds[socket.room].fieldsToFill == 0)  // hasło zostało odgadnięte :)
			{
				console.log(socket.playername, 'win!');

				if (socket.id == socket.room)  // gracz 1.
					rooms[socket.room].p1.score++;
				else
					rooms[socket.room].p2.score++;

				io.sockets.emit('roomUpdate', rooms[socket.room]);
				io.sockets.in(socket.room).emit('secretGuessed', rooms[socket.room]);  // do obydwu graczy
			}
		}
		else
		{
			console.log(socket.playername, 'guessing letter:', letter, '-');

			if (--rounds[socket.room].chances == 0)  // game over x.x
			{
				console.log(socket.playername, 'lose');
				io.sockets.in(socket.room).emit('gameOver', word, rooms[socket.room]);  // do obydwu graczy
			}
		}
	});

});